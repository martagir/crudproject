package users.model;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;

/**
 * Created by ����� on 21.05.2016.
 */

@Transactional
public interface UsersRepository extends JpaRepository<User, Long> {
    Optional<User> findById(long id);
    Page<User> findByName(Pageable pageable, String name);