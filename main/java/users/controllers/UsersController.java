package users.controllers;

import users.model.User;
import users.model.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ����� on 21.05.2016.
 */

@RestController
@CrossOrigin
@RequestMapping(value = "/persons")
public class UsersController {
private static final int PAGE_SIZE = 5;
    @Autowired
    UsersRepository usersRepository;

    @RequestMapping(value = "/page/{page}")
    public Page<User> getPersons(@PathVariable int page){
        PageRequest request =
                new PageRequest(page - 1, PAGE_SIZE, Sort.Direction.ASC,"id");
        return usersRepository.findAll(request);
    }

    @RequestMapping(method = RequestMethod.POST)
    public void addPerson(@RequestBody User user){
        usersRepository.save(user);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void delPerson(Long id){
        usersRepository.delete(id);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public void update(@RequestBody User user){
        User user1 = usersRepository.findById(user.getId()).orElseThrow(() -> new UserNotFoundException(user.getId()));
        user1.setName(user.getName());
        user1.setAge(user.getAge());
        user1.setAdmin(user.isAdmin());
        usersRepository.save(user1);
    }

    @RequestMapping(value = "/{name}", method = RequestMethod.GET)
    public Page<User> findPerson(@PathVariable String name){
        List<User> list = new ArrayList<>();
        PageRequest request =
                new PageRequest(0, PAGE_SIZE, Sort.Direction.ASC, "id");
        //if(usersRepository.findByName(request,name).size() > 0)
            return usersRepository.findByName(request, name);
        //else {throw new UserNotFoundException(name);}
    }
}

@ResponseStatus(HttpStatus.NOT_FOUND)
class UserNotFoundException extends RuntimeException{
    public UserNotFoundException(long id){
        super("could not find user with id'" + id + "' .");
    }
    public UserNotFoundException(String name){
        super("could not find user with name'" + name + "' .");
    }
}