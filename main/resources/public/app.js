



var module = angular.module('myapp', ['ngResource']);

 module.factory('UserFabric', function($resource){
 return $resource('http://localhost:8080/persons/:page/:pageNum',{page:'', pageNum:''},{
     update: {method: 'PUT', params: {id: '@id'}},
     list:{isArray:false,method:'GET',transformResponse: function (data, headers){  return JSON.parse(data); }}
 });
 })
 .controller('UsersController', function($scope, UserFabric){

     /*url functions*/
    var url = function(p, n){
    return {page : p || '', pageNum : n || ''}
    };

    var urlPage = function(pageNumber){
    return {page : 'page', pageNum : pageNumber}
    };

    /* update on the First page*/
    var update = function(){
        $scope.list = UserFabric.list(url('page', '1'));
    };




     /*Add user function*/
    $scope.add = function(viewLastPage, numberOfElementsOnPage, sizeOfElementsOnPage ){
        var record = new UserFabric();
        record.name = $scope.nameAdd;
        record.age = $scope.ageAdd;
        record.admin = $scope.adminAdd;

        record.$save(url(), function(){
            $scope.nameAdd = "";
            $scope.ageAdd = "";
            $scope.adminAdd = "";
            if(viewLastPage == 0)
                {viewLastPage = viewLastPage + 1;}
            if(numberOfElementsOnPage >= sizeOfElementsOnPage)
                {viewLastPage = viewLastPage + 1;}

            $scope.list = UserFabric.list(urlPage(viewLastPage));
        });
    };

     /*Backward list function*/
    $scope.bp = function(cur, t){
    rc = cur+1;

        if(rc > 1)
            $scope.list = UserFabric.list(urlPage(rc-1));

    };

     /*Forward list function*/
     $scope.newUpdate = function(current, total){
     rightCurrent = current + 1;

         if(rightCurrent < total)
            $scope.list = UserFabric.list(urlPage(rightCurrent+1));
    };

     /* Update record function*/
     $scope.update = function(id, currentPage){
         var record = new UserFabric();
         record.id = $scope.idUpd;
         record.name = $scope.nameUpd;
         record.age = $scope.ageUpd;
         record.admin = $scope.adminUpd;
         UserFabric.update(record,  function(){
             $scope.idUpd = "";
             $scope.nameUpd = "";
             $scope.ageUpd = "";
             $scope.adminUpd = "";
             $scope.list = UserFabric.list(urlPage(currentPage));
         });
     };

     /* Find function*/
     $scope.find = function(usernameFind){
     $scope.list = UserFabric.list(url(usernameFind),  function(){});
    };

     /* Reset find result function*/
     $scope.resetFindResult = function(){
         $scope.usernameFind = "";
         update();
     };

     /* Delete function*/
    $scope.delete = function(idDel,currentPage1, checkElementsOnThePage, isFirstPage){
        UserFabric.delete({id:idDel},  function(){
        $scope.idDel = "";
        if(checkElementsOnThePage > 1 || isFirstPage){
            $scope.list = UserFabric.list(urlPage(currentPage1));}
            else{
                $scope.list = UserFabric.list(urlPage(currentPage1-1));}
    });
    };

 update();
 });