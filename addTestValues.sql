CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `age` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `is_admin` bit(1) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert into users (age, created_date, is_admin, name) values (31,'2016-02-19 14:24:53', true, 'user1');
insert into users (age, created_date, is_admin, name) values (32,'2016-02-19 14:24:53', true, 'user2');
insert into users (age, created_date, is_admin, name) values (33,'2016-02-19 14:24:53', true, 'user3');
insert into users (age, created_date, is_admin, name) values (34,'2016-02-19 14:24:53', true, 'user4');
insert into users (age, created_date, is_admin, name) values (35,'2016-02-19 14:24:53', true, 'user5');
insert into users (age, created_date, is_admin, name) values (36,'2016-02-19 14:24:53', false, 'user6');
insert into users (age, created_date, is_admin, name) values (37,'2016-02-19 14:24:53', false, 'user7');
insert into users (age, created_date, is_admin, name) values (38,'2016-02-19 14:24:53', false, 'user8');
insert into users (age, created_date, is_admin, name) values (39,'2016-02-19 14:24:53', false, 'user9');
insert into users (age, created_date, is_admin, name) values (40,'2016-02-19 14:24:53', false, 'user10');
